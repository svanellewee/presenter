package family.stephan.presenter


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_motion.*
import android.os.AsyncTask.execute
import android.support.v7.app.ActionBar

import android.util.Log
import okhttp3.*
import java.io.IOException
import android.util.DisplayMetrics
import kotlin.math.roundToInt


class MotionActivity : AppCompatActivity() {
    var aBar: ActionBar? = null
    val client = OkHttpClient()
    lateinit var address: String
    var height: Float = 0f;
    var width: Float = 0f;
    var hostWidth: Float = 0f;
    var hostHeight: Float = 0f;

    private inline fun getDims() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        this.height = height.toFloat() - activity_motion_event_area.top.toFloat()
        this.width = width.toFloat()
    }

    private inline fun convertXY(x: Int, y: Int): Pair<Int, Int> {
        var newX = (x * (this.hostWidth / this.width)).roundToInt()
        var newY = (y * (this.hostHeight / this.height)).roundToInt()
        //newY += 10 //activity_motion_event_area.top
        return Pair(newX, newY)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_motion)

        val message = intent.getStringExtra(EXTRA_MESSAGE)
        val (hostAddress, hostWidth, hostHeight) = message.split(";")
        this.hostWidth = hostWidth.toFloat()
        this.hostHeight = hostHeight.toFloat()
        getDims()
        supportActionBar!!.hide()
        //aBar!!.hide()
        // Capture the layout's TextView and set the string as its text
        findViewById<TextView>(R.id.textView1).apply {
            text = message
            address = hostAddress
        }
        /*activity_motion_event_area.setOnTouchListener {
            v: View, m: MotionEvent ->
            handleTouch(m)
            true
        }*/

        activity_motion_event_area.setOnTouchListener { v: View, m: MotionEvent ->
            handleTouch(m)

            true
        }

    }

    private fun sendMotion(x: Int, y: Int) {
        var (altX, altY) = convertXY(x, y)
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val content = "{\"x\":\"${altX}\",\"y\":\"${altY}\"}"
        Log.d("sendMotion", "body = ${content} address=${address}")
        val body = RequestBody.create(JSON, content)

        val request = Request.Builder()
                .url("${address}/move/")
                .put(body)
                .build()

        try {
            val response = client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response) = println(response.body()?.string())
            })

            // Do something with the response.
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun handleTouch(m: MotionEvent) {
        val pointerCount = m.pointerCount

        for (i in 0 until pointerCount) {
            val x = m.getX(i)
            val y = m.getY(i)
            val id = m.getPointerId(i)
            val action = m.actionMasked
            val actionIndex = m.actionIndex
            val actionString = when (action) {
                MotionEvent.ACTION_DOWN -> "DOWN"
                MotionEvent.ACTION_UP -> "UP"
                MotionEvent.ACTION_POINTER_DOWN -> "PNTR DOWN"
                MotionEvent.ACTION_POINTER_UP -> "PNTR UP"
                MotionEvent.ACTION_MOVE -> "MOVE"
                else -> ""
            }

            val touchStatus =
                    "Action: $actionString Index: $actionIndex ID: $id X: $x Y: $y"

            if (id == 0) {
                textView1.text = touchStatus
                sendMotion(x.toInt(), y.toInt())
            }

        }
    }
}
