package family.stephan.presenter

import android.content.Intent
import android.gesture.Gesture
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.appcompat.R.id.message
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.zxing.integration.android.IntentIntegrator

const val EXTRA_MESSAGE = "family.stephan.presenter.MESSAGE"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startScan(view: View) {
        val integrator = IntentIntegrator(this@MainActivity)
        integrator.initiateScan()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
        if (scanResult != null) {
            val re = scanResult.contents
            findViewById<TextView>(R.id.textResultView).text = re
            /*val intent = Intent(this, MotionActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, re.toString())
            }
           */val intent = Intent(this, GestureActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, re.toString())
            }
            startActivity(intent)
            Log.d("code", re)
        }
    }
}
