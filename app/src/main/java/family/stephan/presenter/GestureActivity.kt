package family.stephan.presenter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.ActionBar
import android.util.DisplayMetrics
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import kotlinx.android.synthetic.main.activity_gesture.*
import kotlinx.android.synthetic.main.activity_motion.*
import okhttp3.*
import java.io.IOException
import kotlin.math.abs
import kotlin.math.roundToInt

class GestureActivity : AppCompatActivity(), GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    override fun onDoubleTap(e: MotionEvent?): Boolean {
        println("onDoubleTap ${e}")
        sendTap(0,0,true)
        return false // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
        println("onDoubleTapEvent ${e}")

        return true //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
        println("onSingleTapConfirmed ${e}")
        return false  // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowPress(e: MotionEvent?) {
        println("onShowPress ${e}")
        return // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        var horizontalDirection = e?.getX(0)!! - this.centerX
        val horizontal = if (horizontalDirection > 0) {
            "right"
        }else{
            "left"
        }
        var verticalDirection = e?.getY(0)!! - this.centerY
        val vertical = if (verticalDirection < 0) {
            "above"
        }else{
            "below"
        }
        println("onSingleTapUp ${horizontal} ${vertical} ${e}")
        sendTap(verticalDirection.roundToInt(), horizontalDirection.roundToInt(), false)
        return false //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDown(e: MotionEvent?): Boolean {
        println("onDown")

        return false //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        println("onFling ${velocityX} ${velocityY}")
        return false // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        println("onScroll ${distanceX} ${distanceY}")
        sendMotion(e2!!.getX(0), e2!!.getY(0))
        return false //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLongPress(e: MotionEvent?) {
        println("onLongPress")
        return  //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var aBar: ActionBar? = null
    val client = OkHttpClient()
    var address: String? = null
    var height: Float = 0f
    var width: Float = 0f
    var hostWidth: Float = 0f
    var hostHeight: Float = 0f
    var startX: Int = 0
    var startY: Int = 0
    var centerX : Float = 0.0f
    var centerY: Float = 0.0f

    private inline fun getDims() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        this.height = height.toFloat() - gestureControlArea.top.toFloat()
        this.width = width.toFloat()
        this.centerX = this.width / 2
        this.centerY = this.height / 2
    }

    private inline fun convertXY(x: Float, y: Float): Pair<Int, Int> {
        var newX = (x * (this.hostWidth / this.width)).roundToInt()
        var newY = (y * (this.hostHeight / this.height)).roundToInt()
        //newY += 10 //activity_motion_event_area.top
        return Pair(newX, newY)
    }


   // var gDetector: GestureDetectorCompat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gesture)
        this.gDetector = GestureDetectorCompat(this, this)
        gDetector?.setOnDoubleTapListener(this)

        val message = intent.getStringExtra(EXTRA_MESSAGE)
        val (hostAddress, hostWidth, hostHeight) = message.split(";")
        this.hostWidth = hostWidth.toFloat()
        this.hostHeight = hostHeight.toFloat()
        getDims()
        this.address = hostAddress
        supportActionBar!!.hide()
    }

    private fun sendTap(vertical: Int, horizontal: Int, doubleTap: Boolean) {
        //var (altX, altY) = convertXY(x, y)
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val content = if (doubleTap == false) {
            "{\"h\":\"${horizontal}\",\"v\":\"${vertical}\"}"
        } else {
            "{\"d\": \"${doubleTap}\"}"
        }
            Log.d("sendTap", "body = ${content} address=${address}")
            val body = RequestBody.create(JSON, content)

            if (address == null) {
                Log.e("sendTap", "no address")
            } else {
                val request = Request.Builder()
                        .url("${address}/tap/")
                        .put(body)
                        .build()

                try {
                    val response = client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {}
                        override fun onResponse(call: Call, response: Response) = println(response.body()?.string())
                    })

                    // Do something with the response.
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }



    private fun sendMotion(x: Float, y: Float) {
        var (altX, altY) = convertXY(x, y)
        val JSON = MediaType.parse("application/json; charset=utf-8")
        val content = "{\"x\":\"${altX}\",\"y\":\"${altY}\"}"
        Log.d("sendMotion", "body = ${content} address=${address}")
        val body = RequestBody.create(JSON, content)

        if (address == null) {
            Log.e("sendMotion", "no address")
        } else {
            val request = Request.Builder()
                    .url("${address}/move/")
                    .put(body)
                    .build()

            try {
                val response = client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {}
                    override fun onResponse(call: Call, response: Response) = println(response.body()?.string())
                })

                // Do something with the response.
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }

    lateinit var gDetector: GestureDetectorCompat;
    override fun onTouchEvent(event: MotionEvent): Boolean {
        this.gDetector.onTouchEvent(event)
        // Be sure to call the superclass implementation
        /*touchStatusText.text = "touchStatus ${event.toString()}"
        sendMotion(event.getX(0), event.getY(0))
        return super.onTouchEvent(event)*/

        return super.onTouchEvent(event)
    }
}
